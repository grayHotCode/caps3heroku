const mongoose = require('mongoose')
const userSchema = new mongoose.Schema({

	email : {
		type: String,
		required : [true, 'Email is required']
	},

	password : {
		type: String,
		required : [true, 'Password is required']
	},

	// firstName : {
	// 	type: String,
	// },

	// lastName : {
	// 	type: String,
	// },

	// address : {
	// 	type: String,
	// },
	
	isAdmin : {
		type: Boolean,
		default : false
	},

	order: [
	{
		orderId: {
			type: String,
			required : [true, 'order Id is required']
		},

		totalAmount: {
			type: Number,
			required : [true, 'Total Amount is is required']
		}
	}]

	
})

module.exports = mongoose.model('User', userSchema)